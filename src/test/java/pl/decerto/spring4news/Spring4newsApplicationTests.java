package pl.decerto.spring4news;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.decerto.spring4news.Spring4newsApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Spring4newsApplication.class)
public class Spring4newsApplicationTests {

	@Test
	public void contextLoads() {
	}

}
