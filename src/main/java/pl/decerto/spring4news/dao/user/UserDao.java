package pl.decerto.spring4news.dao.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.decerto.spring4news.model.user.UserEntity;

@Repository
public interface UserDao extends CrudRepository<UserEntity, Long> {

	public UserEntity findOneByName(String name);
}
