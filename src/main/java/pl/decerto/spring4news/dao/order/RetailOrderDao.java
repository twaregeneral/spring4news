package pl.decerto.spring4news.dao.order;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.decerto.spring4news.model.order.RetailOrderEntity;

@Repository
public interface RetailOrderDao extends CrudRepository<RetailOrderEntity, Long> {

}
