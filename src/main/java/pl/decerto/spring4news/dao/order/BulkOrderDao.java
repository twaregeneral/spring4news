package pl.decerto.spring4news.dao.order;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.decerto.spring4news.model.order.BulkOrderEntity;

@Repository
public interface BulkOrderDao extends CrudRepository<BulkOrderEntity, Long> {

}
