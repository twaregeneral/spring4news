package pl.decerto.spring4news.service.order.creator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import pl.decerto.spring4news.dao.order.BulkOrderDao;
import pl.decerto.spring4news.dao.user.UserDao;
import pl.decerto.spring4news.model.order.BulkOrderEntity;
import pl.decerto.spring4news.model.user.UserEntity;

import com.google.common.base.Preconditions;

// TODO must not be created on the 1st day of any month
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BulkOrderCreator {

	@Autowired
	private UserDao userDao;

	@Autowired
	private BulkOrderDao orderDao;

	private long userId;

	private BulkOrderCreator(Long userId) {
		this.userId = userId;
	}

	public BulkOrderEntity create() {
		UserEntity user = userDao.findOne(userId);
		Preconditions.checkArgument(user != null, "User of id %s doesn't exist", userId);

		BulkOrderEntity order = new BulkOrderEntity();
		// do smth
		order = orderDao.save(order);
		return order;
	}
}
