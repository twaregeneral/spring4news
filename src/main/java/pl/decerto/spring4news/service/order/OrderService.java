package pl.decerto.spring4news.service.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import pl.decerto.spring4news.model.order.BulkOrderEntity;
import pl.decerto.spring4news.model.order.RetailOrderEntity;
import pl.decerto.spring4news.service.order.creator.BulkOrderCreator;
import pl.decerto.spring4news.service.order.creator.RetailOrderCreator;

@Component
public class OrderService {

	@Autowired
	private ApplicationContext context;

	public BulkOrderEntity createBulkOrder(long userId) {
		BulkOrderCreator creator = context.getBean(BulkOrderCreator.class, userId);
		return creator.create();
	}

	public RetailOrderEntity createRetailOrder(long userId) {
		RetailOrderCreator creator = context.getBean(RetailOrderCreator.class, userId);
		return creator.create();
	}
}
