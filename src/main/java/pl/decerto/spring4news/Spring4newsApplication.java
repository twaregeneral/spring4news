package pl.decerto.spring4news;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spring4newsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Spring4newsApplication.class, args);
	}
}
