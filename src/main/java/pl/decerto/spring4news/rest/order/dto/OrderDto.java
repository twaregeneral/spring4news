package pl.decerto.spring4news.rest.order.dto;

public class OrderDto {

	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
