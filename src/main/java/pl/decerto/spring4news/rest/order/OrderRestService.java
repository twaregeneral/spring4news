package pl.decerto.spring4news.rest.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.decerto.spring4news.dao.order.BulkOrderDao;
import pl.decerto.spring4news.dao.order.RetailOrderDao;
import pl.decerto.spring4news.model.order.BaseOrder;
import pl.decerto.spring4news.model.order.BulkOrderEntity;
import pl.decerto.spring4news.model.order.RetailOrderEntity;
import pl.decerto.spring4news.rest.order.dto.OrderDto;
import pl.decerto.spring4news.service.order.OrderService;

import com.google.common.base.Preconditions;

@Controller
public class OrderRestService {

	@Autowired
	private RetailOrderDao retailOrderDao;

	@Autowired
	private BulkOrderDao bulkOrderDao;

	@Autowired
	private OrderService service;

	@RequestMapping("/retail-order/find-by-id")
	@ResponseBody
	public OrderDto findRetailOrderById(@RequestParam Long id) {
		RetailOrderEntity order = retailOrderDao.findOne(id);
		Preconditions.checkArgument(order != null, "Order of id %s not found", id);
		return toDto(order);
	}

	@RequestMapping("/bulk-order/find-by-id")
	@ResponseBody
	public OrderDto findBulkOrderById(@RequestParam Long id) {
		BulkOrderEntity order = bulkOrderDao.findOne(id);
		Preconditions.checkArgument(order != null, "Order of id %s not found", id);
		return toDto(order);
	}

	@RequestMapping("/retail-order/create")
	@ResponseBody
	public OrderDto createRetailOrder(@RequestParam(name = "user-id") Long userId) {
		RetailOrderEntity order = service.createRetailOrder(userId);
		return toDto(order);
	}

	@RequestMapping("/bulk-order/create")
	@ResponseBody
	public OrderDto createBulkOrder(@RequestParam(name = "user-id") Long userId) {
		BulkOrderEntity order = service.createBulkOrder(userId);
		return toDto(order);
	}

	private OrderDto toDto(BaseOrder order) {
		OrderDto dto = new OrderDto();
		dto.setId(order.getId());
		return dto;
	}

}
