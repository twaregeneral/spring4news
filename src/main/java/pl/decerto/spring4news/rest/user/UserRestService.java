package pl.decerto.spring4news.rest.user;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.decerto.spring4news.dao.user.UserDao;
import pl.decerto.spring4news.model.user.UserEntity;
import pl.decerto.spring4news.rest.user.dto.UserDto;

import com.google.common.base.Preconditions;

@Controller
@RequestMapping("/user")
public class UserRestService {

	@Autowired
	private UserDao dao;

	@RequestMapping("/find-by-id")
	@ResponseBody
	public UserDto findUser(@RequestParam Long id) {
		UserEntity user = dao.findOne(id);
		Preconditions.checkArgument(user != null, "User of id %s not found", id);
		return toDto(user);
	}

	@RequestMapping("/find-by-name")
	@ResponseBody
	public UserDto findUser(@RequestParam String name) {
		UserEntity user = dao.findOneByName(name);
		Preconditions.checkArgument(user != null, "User of name %s not found", name);
		return toDto(user);
	}

	@RequestMapping("/create")
	@ResponseBody
	public UserDto createUser(@RequestParam(required = false) String name) {
		if (name != null) {
			UserEntity alreadyExistingUser = dao.findOneByName(name);
			Preconditions.checkArgument(alreadyExistingUser == null, "User of name %s already exists", name);
		} else {
			name = generateRandomName();
		}

		UserEntity user = new UserEntity();
		user.setName(name);
		user = dao.save(user);

		return toDto(user);
	}

	private String generateRandomName() {
		String randomName;
		do {
			randomName = UUID.randomUUID().toString();
		} while (dao.findOneByName(randomName) != null);
		return randomName;
	}

	private UserDto toDto(UserEntity entity) {
		UserDto dto = new UserDto();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		return dto;
	}

}
