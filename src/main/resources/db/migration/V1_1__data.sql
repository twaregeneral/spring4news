insert into user(id, name) values(1, 'Tomek');
insert into user(id, name) values(2, 'Marek');
insert into user(id, name) values(3, 'Adam');

insert into bulk_order(id) values(1);
insert into bulk_order(id) values(2);
insert into bulk_order(id) values(3);

insert into retail_order(id) values(1);
insert into retail_order(id) values(2);
insert into retail_order(id) values(3);